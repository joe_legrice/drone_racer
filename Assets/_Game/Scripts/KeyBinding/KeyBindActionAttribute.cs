﻿using UnityEngine;
using System;

[AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
public class KeyBindActionAttribute : Attribute
{
    private string m_actionIdentifier;

    public string ActionIdentifier { get { return m_actionIdentifier; } }

    public KeyBindActionAttribute(string actionIdentifier)
    {
        m_actionIdentifier = actionIdentifier;
    }
}


public class KeyBindIdentifierAttribute : PropertyAttribute
{
}