﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;

public class KeyBindingManager : Singleton<KeyBindingManager>
{
    [SerializeField] private List<KeyBindingAction> m_allKeyBindingActions;

    private List<KeyBindingAction> m_pressedKeys = new List<KeyBindingAction>();

    public List<T> GetAllActions<T>(string actionType) where T : KeyBindingAction
    {
        return m_allKeyBindingActions.Where(kba => {
            KeyBindActionAttribute kbaa = (KeyBindActionAttribute)Attribute.GetCustomAttribute(kba.GetType(), typeof(KeyBindActionAttribute));
            return kbaa.ActionIdentifier == actionType && kba.GetType() == typeof(T);
        }).Select(kba => {
            return (T)kba;
        }).ToList();
    }

    private void Update()
    {
        foreach (KeyBindingAction kba in m_allKeyBindingActions)
        {
            if (Input.GetKeyDown(kba.GetKeyBound()))
            {
                m_pressedKeys.Add(kba);
                kba.KeyWasPressed();

                foreach (KeyBindingAction pressedKey in m_pressedKeys.GetRange(0, m_pressedKeys.Count))
                {
                    if (pressedKey.IsCancelledBy(kba))
                    {
                        pressedKey.KeyCancelled();
                        m_pressedKeys.Remove(pressedKey);
                    }
                }
            }
            else if (Input.GetKeyUp(kba.GetKeyBound()) && m_pressedKeys.Contains(kba))
            {
                m_pressedKeys.Remove(kba);
                kba.KeyWasReleased();
            }
            else
            {
                if (Input.GetKey(kba.GetKeyBound()) && m_pressedKeys.Contains(kba))
                {
                    kba.KeyIsPressed();
                }
                else
                {
                    kba.KeyIsReleased();
                }
            }
        }
	}
}
