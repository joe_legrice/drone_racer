﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public abstract class KeyBindingAction : ScriptableObject
{
    [SerializeField] private KeyCode m_keyBound;
    [SerializeField] private List<KeyBindingAction> m_cancelledBy;

    public KeyCode GetKeyBound()
    {
        return m_keyBound;
    }

    public bool IsCancelledBy(KeyBindingAction kba)
    {
        return m_cancelledBy.Contains(kba);
    }

    public bool IsPressed { get; private set; }

    public virtual void KeyWasPressed() { IsPressed = true; }
    public virtual void KeyWasReleased() { IsPressed = false; }
    public virtual void KeyIsPressed() { }
    public virtual void KeyIsReleased() { }
    public virtual void KeyCancelled() { IsPressed = false; }
}
