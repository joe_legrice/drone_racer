﻿using UnityEngine;


public class DroneConfigurationController : MonoBehaviour
{
    [SerializeField] private DroneConfiguration m_configuration;

    [SerializeField] private DroneCameraController m_cameraController;
    [SerializeField] private DroneMovementController m_movementController;
    [SerializeField] private DroneRotationController m_rotationController;

    private void Awake()
    {
        m_cameraController.Initialize(m_configuration);
        m_movementController.Initialize(m_configuration);
        m_rotationController.Initialize(m_configuration);
    }
}
