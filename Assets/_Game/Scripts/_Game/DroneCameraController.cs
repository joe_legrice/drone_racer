﻿using UnityEngine;

public class DroneCameraController : DroneConfigReceiver
{
    [SerializeField] private Transform m_droneRotationTransform;
    [SerializeField] private Transform m_yawRotationTransform;
    [SerializeField] private Transform m_pitchRotationTransform;

    private void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        Vector2 mouseDelta = new Vector2(
            -Input.GetAxis("Mouse X"),
            -Input.GetAxis("Mouse Y")
        );

        Vector3 rightDelta = m_yawRotationTransform.forward + Configuration.MouseSensitivity * Time.deltaTime * -mouseDelta.x * m_yawRotationTransform.right;
        m_yawRotationTransform.rotation = Quaternion.LookRotation(m_yawRotationTransform.forward + rightDelta);
        
        Vector3 upDelta = m_pitchRotationTransform.forward + Configuration.MouseSensitivity * Time.deltaTime * -mouseDelta.y * m_pitchRotationTransform.up;
        Vector3 newUpForward = m_pitchRotationTransform.forward + upDelta;
        Quaternion clampedForward = Quaternion.LookRotation(newUpForward, m_droneRotationTransform.up);

        float angle = Quaternion.Angle(clampedForward, Quaternion.LookRotation(m_yawRotationTransform.up, -m_yawRotationTransform.forward));
        if (angle >= Configuration.UpperClampAngle && angle <= Configuration.LowerClampAngle)
        {
            m_pitchRotationTransform.rotation = clampedForward;
        }
    }
}
