﻿using UnityEngine;

public class DroneConfigReceiver : MonoBehaviour
{
    private DroneConfiguration m_configuration;

    public DroneConfiguration Configuration { get { return m_configuration; } }

    public virtual void Initialize(DroneConfiguration dc)
    {
        m_configuration = dc;
    }
}
