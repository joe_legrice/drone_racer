﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class DroneRotationController : DroneConfigReceiver
{
    [SerializeField] private string m_actionType = "Rotation";
    [SerializeField] private Transform m_droneTransform;
    [SerializeField] private Transform m_lookTransform;
    
    private List<RotationKeyBindingAction> m_keyBoundActions;
    
    private void Start()
    {
        m_keyBoundActions = KeyBindingManager.Instance.GetAllActions<RotationKeyBindingAction>(m_actionType);
    }

    private void Update()
    {
        Quaternion targetRotation = Quaternion.identity;
        foreach (RotationKeyBindingAction kba in m_keyBoundActions)
        {
            targetRotation *= kba.GetRotation(Configuration.LeanRotationMultiplier);
        }
        m_droneTransform.localRotation = Quaternion.Lerp(m_droneTransform.localRotation, targetRotation, Configuration.LeanRotationLerpSpeed * Time.deltaTime);
    }
}
