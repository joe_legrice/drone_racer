﻿using UnityEngine;
using System.Collections.Generic;

public class DroneMovementController : DroneConfigReceiver
{
    [SerializeField] private string m_keyBindIdentifier = "Movement";
    [SerializeField] private Transform m_lookTransform;
    [SerializeField] private Rigidbody m_rigidbody;
    
    private float m_breakingTime;
    private List<MovementKeyBindingAction> m_movementActions;

    private void Start()
    {
        m_movementActions = KeyBindingManager.Instance.GetAllActions<MovementKeyBindingAction>(m_keyBindIdentifier);
    }

    private void FixedUpdate()
    {
        Vector3 counterGravity = Physics.gravity.magnitude * Vector3.up * m_rigidbody.mass;

        bool isAccelerating = false;
        bool isAcceleratingVertically = false;
        Vector3 forceVector = Vector3.zero;
        foreach (MovementKeyBindingAction mkba in m_movementActions)
        {
            isAccelerating |= mkba.IsPressed;
            isAcceleratingVertically |= mkba.IsVerticalMovement && mkba.IsPressed;

            forceVector += m_lookTransform.rotation * mkba.GetForceVector(Configuration.MaximumAccelerationForce, Configuration.AccelerationCurve);
        }

        Vector3 breakingForce = Vector3.zero;
        if (!isAcceleratingVertically)
        {
            breakingForce += -m_rigidbody.velocity.y * Vector3.up * m_rigidbody.mass * Configuration.VerticalBrake;
        }

        if (isAccelerating)
        {
            m_breakingTime += Time.fixedDeltaTime;
            float acceleration = Configuration.GetBreakingDeceleration(m_breakingTime);
            breakingForce += -m_rigidbody.velocity.normalized * m_rigidbody.mass * acceleration;
        }
        else
        {
            m_breakingTime = 0.0f;
        }

        m_rigidbody.AddForce(forceVector + counterGravity + breakingForce);
    }
}

