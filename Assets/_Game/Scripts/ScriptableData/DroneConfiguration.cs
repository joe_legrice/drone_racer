﻿using UnityEngine;

[System.Serializable]
[CreateAssetMenu()]
public class DroneConfiguration : ScriptableObject
{
    // DroneMovementController
    [SerializeField] private float m_verticalBrake;
    [SerializeField] private float m_breakingDeceleration;
    [SerializeField] private AnimationCurve m_breakingDecelerationCurve;
    [SerializeField] private float m_maximumAccelerationForce = 5.0f;
    [SerializeField] private AnimationCurve m_droneAccelerationCurve;

    // DroneRotationController
    [SerializeField] private float m_leanRotationMultiplier;
    [SerializeField] private float m_leanRotationLerpSpeed;

    // DroneCameraController
    [SerializeField] private float m_mouseSensitivity;
    [SerializeField] private float m_upperClampAngle;
    [SerializeField] private float m_lowerClampAngle;

    // DroneRotationController
    public float LeanRotationMultiplier { get { return m_leanRotationLerpSpeed; } }
    public float LeanRotationLerpSpeed { get { return m_leanRotationLerpSpeed; } }

    //DroneCameraController
    public float MouseSensitivity { get { return m_mouseSensitivity; } }
    public float UpperClampAngle { get { return m_upperClampAngle; } }
    public float LowerClampAngle { get { return m_lowerClampAngle; } }

    // DroneMovementController
    public float MaximumAccelerationForce { get { return m_maximumAccelerationForce; } }
    public AnimationCurve AccelerationCurve { get { return m_droneAccelerationCurve; } }
    public float VerticalBrake { get { return m_verticalBrake; } }

    public float GetBreakingDeceleration(float time)
    {
        return m_breakingDeceleration * m_breakingDecelerationCurve.Evaluate(time);
    }
}
