﻿using UnityEngine;

[System.Serializable]
[KeyBindAction("Movement")]
[CreateAssetMenu(fileName = "NewMovementBindingAction", menuName ="Key Binding Action/Movement")]
public class MovementKeyBindingAction : KeyBindingAction
{
    [SerializeField] private bool m_isVerticalMovement;
    [SerializeField] private Vector3 m_movementDirection;

    [System.NonSerialized] private float m_timePassed;

    public bool IsVerticalMovement { get { return m_isVerticalMovement; } }

    public Vector3 GetForceVector(float maxAccelerationForce, AnimationCurve accelerationCurve)
    {
        float acceleration = accelerationCurve.Evaluate(m_timePassed);
        return m_movementDirection * maxAccelerationForce * acceleration;
    }

    public override void KeyIsPressed()
    {
        base.KeyIsPressed();
        m_timePassed += Time.deltaTime;
    }
    
    public override void KeyWasReleased()
    {
        base.KeyWasReleased();
        m_timePassed = 0.0f;
    }

    public override void KeyCancelled()
    {
        base.KeyCancelled();
        m_timePassed = 0.0f;
    }
}
