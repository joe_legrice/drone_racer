﻿using UnityEngine;
using System.Collections;

[System.Serializable]
[KeyBindAction("Rotation")]
[CreateAssetMenu(fileName = "NewRotationBindingAction", menuName = "Key Binding Action/Rotation")]
public class RotationKeyBindingAction : KeyBindingAction
{
    [SerializeField] private Vector3 m_eulerRotation;

    public Quaternion GetRotation(float multiplier)
    {
        if (IsPressed)
        {
            return Quaternion.Euler(multiplier * m_eulerRotation);
        }
        else
        {
            return Quaternion.identity;
        }
    }
}
